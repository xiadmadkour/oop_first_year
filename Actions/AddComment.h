#pragma once

#include "Action.h"

class AddComment :
	public Action
{
private:
	Point Position;
public:
	AddComment(ApplicationManager *pAppManager);


	virtual void ReadActionParameters();
	
	virtual void Execute() ;
};

