#include "AddComment.h"

#include "..\ApplicationManager.h"

#include "..\GUI\input.h"
#include "..\GUI\Output.h"

//#include <sstream>

using namespace std;

//constructor: set the ApplicationManager pointer inside this action
AddComment::AddComment(ApplicationManager *pAppManager)
	:Action(pAppManager)
{}

void AddComment::ReadActionParameters()
{
	Input *pIn = pManager->GetInput();
	Output *pOut = pManager->GetOutput();
	
	pOut->PrintMessage("Simple Value Assignment Statement: Click to add the statement");

	pIn->GetPointClicked(Position);
	pOut->ClearStatusBar();		
}

void addcomment::execute()
{
	readactionparameters();
		
	
	//calculating left corner of assignement statement block
	point corner;
	corner.x = position.x - ui.assgn_wdth/2;
	corner.y = position.y ;
	
	smplassign *passign = new smplassign(corner, "", 0);

	pManager->AddStatement(pAssign);
}

