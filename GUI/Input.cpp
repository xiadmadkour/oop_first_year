#include "Input.h"
#include "Output.h"

Input::Input(window* pW)
{
	pWind = pW; //point to the passed window
}

void Input::GetPointClicked(Point &P) const
{
	pWind->WaitMouseClick(P.x, P.y);	//Wait for mouse click
}


string Input::GetString(Output *pO) const 
{
	string Str;
	char Key;
	while(1)
	{
		pWind->WaitKeyPress(Key);
		if(Key == 27 )	//ESCAPE key is pressed
			return "";	//returns nothing as user has cancelled label
		if(Key == 13 )	//ENTER key is pressed
			return Str;
		//Key="";

		if(Key == 8 &&Str.size()>0)	//BackSpace is pressed
			Str.resize(Str.size() -1 );			
		else if(Key != 8)
			Str += Key;
		pO->PrintMessage(Str);
	}
	
}

ActionType Input::GetUserAction() const
{	
	//This function reads the position where the user clicks to determine the desired action

	int x,y;
	pWind->WaitMouseClick(x, y);	//Get the coordinates of the user click

	if(UI.AppMode == DESIGN )	//application is in design mode
	{
		//[1] If user clicks on the Toolbar
		if ( y >= 0 && y < UI.TlBrWdth)
		{	//Check whick Menu item was clicked

			if( x >= 0 && x < UI.MnItWdth  )
				return ADD_SMPL_ASSIGN;	//add assignment
			if( x >= UI.MnItWdth && x < 2* UI.MnItWdth   )		
				return ADD_StartEnd;	//add start-end
			if( x >= 2*UI.MnItWdth && x < 3* UI.MnItWdth )		
				return ADD_ReadWrite;	//add read-write
			if( x >= 3*UI.MnItWdth && x < 4* UI.MnItWdth )		
				return ADD_CONDITION;	//add condition
			if( x >= 4*UI.MnItWdth && x < 5* UI.MnItWdth )		
				return ADD_CONNECTOR;	//add connector
			if( x >= 5*UI.MnItWdth && x < 6* UI.MnItWdth )		
				return EDIT_STAT;		//edit a statement text	
			if( x >= 6*UI.MnItWdth && x < 7* UI.MnItWdth )		
				return EDIT_CONNECTOR;	//edit_connector
			if( x >= 7*UI.MnItWdth && x < 8* UI.MnItWdth )		
				return ADD_COMMENT;		//add comment
			if( x >= 8*UI.MnItWdth && x < 9* UI.MnItWdth )		
				return DEL;				//Delete statement-connector	
			if( x >= 9*UI.MnItWdth && x < 10* UI.MnItWdth )		
				return SELECT;			//Selects a statment-connector
			if( x >= 10*UI.MnItWdth && x < 11* UI.MnItWdth )		
				return MOVE;			//moves a statement
			if( x >= 11*UI.MnItWdth && x < 12* UI.MnItWdth )		
				return CUT;				//Cuts a statement
			if( x >= 12*UI.MnItWdth && x < 13* UI.MnItWdth )		
				return COPY;			//copies a statement
			if( x >= 13*UI.MnItWdth && x < 14* UI.MnItWdth )		
				return PASTE;			//pastes a statement
			if( x >= 14*UI.MnItWdth && x < 15* UI.MnItWdth )		
				return SAVE;			//saves the program
			if( x >= 15*UI.MnItWdth && x < 16* UI.MnItWdth )		
				return LOAD;			//loads the program
			if( x >= 16*UI.MnItWdth && x < 17* UI.MnItWdth )		
				return SIM_MODE;		//switch to simulation mode
			if( x >= 17*UI.MnItWdth && x < 18* UI.MnItWdth )		
				return EXIT;			//exits the program

			return DSN_TOOL;	//a click on empty part of the tool bar
		
		}
		
		//[2] User clicks on the drawing area
		if ( y >= UI.TlBrWdth && y < UI.height - UI.StBrWdth)
		{
			return DRAWING_AREA;	//user want to select/unselect a statement in the flowchart
		}
		
		//[3] User clicks on the status bar
		return STATUS;
	}
	else	//Application is in Simulation mode
	{
		if ( y >= 0 && y < UI.TlBrWdth)
		{	//Check whick Menu item was clicked

			if( x >= 0 && x < UI.MnItWdth  )
				return SIMULATE;	//add assignment
			if( x >= UI.MnItWdth && x < 2* UI.MnItWdth   )		
				return STOP;	//add start-end
			if( x >= 2*UI.MnItWdth && x < 3* UI.MnItWdth   )		
				return GENERATE;	//add start-end
			if( x >= 3*UI.MnItWdth && x < 4* UI.MnItWdth )		
				return DSN_MODE;	//add read-write
			if( x >= 4*UI.MnItWdth && x < 5* UI.MnItWdth )		
				return EXIT;			//exits the program

			return SIM_TOOL;
		}

		if ( y >= UI.height - UI.StBrWdth && y < UI.height )
		{
			return STATUS;	
		}
		

	}

}

double Input::GetValue(Output* pO) const	// Reads a double value from the user 
{
	pO->PrintMessage("Please enter a value"); 
	string input = GetString(pO) ;
	double d = stod(input) ;
	
	for(int i = 0 ; i<input.length() ; i++)
	{
		if(!isdigit(input[i]) && input[i]!='.')
			GetValue(pO) ;
	}

	return d;
}



Input::~Input()
{}
