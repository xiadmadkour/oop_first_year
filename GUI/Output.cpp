#include "Output.h"


Output::Output()
{
	//Initialize user interface parameters
	UI.DSN_BUT_NO=18;
	UI.MnItWdth = 60;

	UI.width = UI.DSN_BUT_NO*UI.MnItWdth+15;
	UI.height = 700;
	UI.wx = 15;
	UI.wy =15;

	UI.AppMode = DESIGN;	//Design Mode is the default mode

	UI.StBrWdth = 50;
	UI.TlBrWdth = 50;

	UI.DrawClr = BLUE;
	UI.HiClr = RED;
	UI.MsgClr = RED;

	UI.ASSGN_WDTH = 150;
	UI.ASSGN_HI = 50;

	UI.START_RAD = 40 ;

	UI.COND_WDTH = 150 ;
	UI.COND_HI = 50 ;

	UI.CON_WDTH = 20 ;
	UI.COND_HI = 30 ;

	//Create the output window
	pWind = CreateWind(UI.width, UI.height, UI.wx, UI.wy);
	//Change the title
	pWind->ChangeTitle("Programming Techniques Project");
	
	pWind->SetPen(RED,3);
	CreateDesignToolBar();
	CreateStatusBar();

}


Input* Output::CreateInput()
{
	Input* pIn = new Input(pWind);
	return pIn;
}

//======================================================================================//
//								Interface Functions										//
//======================================================================================//

window* Output::CreateWind(int wd, int h, int x, int y)
{
	return new window(wd, h, x, y);
}
//////////////////////////////////////////////////////////////////////////////////////////
void Output::CreateStatusBar()
{
	pWind->DrawLine(0, UI.height-UI.StBrWdth, UI.width, UI.height-UI.StBrWdth);

}
//////////////////////////////////////////////////////////////////////////////////////////
void Output::CreateDesignToolBar()
{
	UI.AppMode = DESIGN;	//Design Mode
	pWind->SetPen(RED, 1);
	pWind->SetBrush(WHITE);
	pWind->DrawRectangle(0, 0, UI.width,UI.TlBrWdth);
	//fill the tool bar 
	//You can draw the tool bar icons in any way you want.
	//This can be better done by storing the images name in an array then draw the toolbar using a "for" loop
	
	string name[]={"images\\Asin.jpg","images\\Circ.jpg","images\\Pgrm.jpg","images\\Cond.jpg",
		"images\\Conn.jpg","images\\edtasin.jpg","images\\edtconn.jpg","images\\cmnt.jpg",
		"images\\del.jpg","images\\select.jpg","images\\move.jpg",
	"images\\cut.jpg","images\\copy.jpg","images\\paste.jpg","images\\save.jpg",
	"images\\load.jpg","images\\sim_bar.jpg",
	"images\\Exit.jpg"};
	for (int i = 0; i < UI.DSN_BUT_NO; i++)
	{
		pWind->DrawImage(name[i],i * UI.MnItWdth, 0);
	}

	//Draw a line under the toolbar
	pWind->DrawLine(0, UI.TlBrWdth, UI.width, UI.TlBrWdth);	

}

void Output::CreateSimulationToolBar()
{
	UI.AppMode = SIMULATION;	//Simulation Mode
	///TODO: add code to create the simulation tool bar
	pWind->SetPen(RED, 1);
	pWind->SetBrush(WHITE);
	pWind->DrawRectangle(0, 0, UI.width,UI.TlBrWdth);
	int i=0;	
	//fill the tool bar 
	//You can draw the tool bar icons in any way you want.
	//This can be better done by storing the images name in an array then draw the toolbar using a "for" loop
	pWind->DrawImage("images\\Sim.jpg",i++ * UI.MnItWdth, 0);
	pWind->DrawImage("images\\Stop.jpg", i++ * UI.MnItWdth, 0);
	pWind->DrawImage("images\\gen.jpg", i++ * UI.MnItWdth, 0);
	pWind->DrawImage("images\\design.jpg", i++ * UI.MnItWdth, 0);
	pWind->DrawImage("images\\Exit.jpg", i++ * UI.MnItWdth, 0);

	//Draw a line under the toolbar
	pWind->DrawLine(0, UI.TlBrWdth, UI.width, UI.TlBrWdth);	
	

}
//////////////////////////////////////////////////////////////////////////////////////////
void Output::ClearStatusBar()
{
	//Clear Status bar by drawing a filled white rectangle
	pWind->SetPen(RED, 1);
	pWind->SetBrush(WHITE);
	pWind->DrawRectangle(0, UI.height - UI.StBrWdth, UI.width, UI.height);
}
//////////////////////////////////////////////////////////////////////////////////////////
void Output::ClearDrawArea()
{
	pWind->SetPen(RED, 1);
	pWind->SetBrush(WHITE);
	pWind->DrawRectangle(0, UI.TlBrWdth, UI.width, UI.height - UI.StBrWdth);
	
}
//////////////////////////////////////////////////////////////////////////////////////////
void Output::PrintMessage(string msg)	//Prints a message on status bar
{
	ClearStatusBar();	//First clear the status bar
	
	pWind->SetPen(UI.MsgClr, 50);
	pWind->SetFont(20, BOLD , BY_NAME, "Arial");   
	pWind->DrawString(10, UI.height - UI.StBrWdth/1.5, msg);
}

//======================================================================================//
//								Statements Drawing Functions								//
//======================================================================================//

//Draw assignment statement and write the "Text" on it
void Output::DrawAssign(Point Left, int width, int height, string Text, bool Selected)
{
	if(Selected)	//if stat is selected, it should be highlighted
		pWind->SetPen(UI.HiClr,3);	//use highlighting color
	else
		pWind->SetPen(UI.DrawClr,3);	//use normal color

	//Draw the statement block rectangle
	pWind->DrawRectangle(Left.x, Left.y, Left.x + width, Left.y + height);
		
	//Write statement text
	pWind->SetPen(BLACK, 2);
	pWind->DrawString(Left.x+width/4, Left.y + height/4, Text);

}
//////////////////////////////////////////////////////////////////////////////////////////

//Draw I/O statement and write the "Text" on it
void Output::DrawReadWrite(Point Left, int width, int height, string Text, bool Selected)
{
	if(Selected)	//if stat is selected, it should be highlighted
		pWind->SetPen(UI.HiClr,3);	//use highlighting color
	else
		pWind->SetPen(UI.DrawClr,3);	//use normal color

	//Draw the read/write block parallelogram
	int XVerts[] = {Left.x, Left.x+width, Left.x+width-12 ,Left.x - 12} ; 
	int YVerts[] = {Left.y, Left.y,Left.y+height, Left.y+height} ; 
	pWind->DrawPolygon(XVerts, YVerts,4) ;
		
	//Write statement text
	pWind->SetPen(BLACK, 2);
	pWind->DrawString(Left.x+width/10, Left.y + height/4, Text);

}
//////////////////////////////////////////////////////////////////////////////////////////

//Draw I/O statement and write the "Text" on it
void Output::DrawStartEnd(Point center, int radius, string Text, bool Selected)
{
	if(Selected)	//if stat is selected, it should be highlighted
		pWind->SetPen(UI.HiClr,3);	//use highlighting color
	else
		pWind->SetPen(UI.DrawClr,3);	//use normal color

	//Draw the start/end block circle
	pWind->DrawCircle(center.x, center.y, radius, FRAME) ; 
		
	//Write statement text
	pWind->SetPen(BLACK, 2);
	pWind->DrawString(center.x-radius/4, center.y - radius/4, Text);

}
//////////////////////////////////////////////////////////////////////////////////////////

//Draw I/O statement and write the "Text" on it
void Output::DrawConditional(Point Left, int width, int height, string Text, bool Selected)
{
	if(Selected)	//if stat is selected, it should be highlighted
		pWind->SetPen(UI.HiClr,3);	//use highlighting color
	else
		pWind->SetPen(UI.DrawClr,3);	//use normal color

	//Draw the start/end block circle
	int XVerts[] = {Left.x, Left.x + width/2, Left.x, Left.x - width/2} ;
	int YVerts[] = {Left.y, Left.y + height/2,Left.y+height, Left.y+height/2} ;
	pWind->DrawPolygon(XVerts, YVerts, 4) ;

	//Write statement text
	pWind->SetPen(BLACK, 2);
	pWind->DrawString(Left.x - width/5 , Left.y + height/4, Text);

}
//////////////////////////////////////////////////////////////////////////////////////////

void Output :: DrawSimpleConnector(Point st , Point end  , bool Selected) 
{
	if(Selected)	
		pWind->SetPen(UI.HiClr,3);	
	else
		pWind->SetPen(UI.DrawClr,3);

	pWind->DrawLine(st.x , st.y , end.x , st.y  ) ;
	pWind->DrawLine( end.x , st.y ,end.x , end.y ) ;

	if(st.y==end.y)
	{
		if(st.x < end.x )
			DrawHorizontalArrow( end , Selected , true) ;
		else 
			DrawHorizontalArrow( end , Selected , false) ;
	}
	else
		DrawVerticalArrow(end , Selected) ;
}

void Output :: DrawConnector(Point st , Point end  , bool Selected)
{
	if(Selected)	
		pWind->SetPen(UI.HiClr,3);	
	else
		pWind->SetPen(UI.DrawClr,3);

	
	if(end.y<st.y || (st.x == end.x && st.y==end.y) )
	{
		int x=end.x-st.x;
		Point P[6];
		int y=end.y-st.y;
		

		P[0].x=st.x;
		P[0].y=st.y;
		
		P[1].x=P[0].x;
		int z=x!=0? x/abs(x):1;

		if (y>0)
		{
			P[1].y=(st.y+end.y)/2;
			P[2].x=end.x;
		}
		else
		{	
			P[1].y=P[0].y+10;
			P[2].x=st.x+(x > UI.COND_WDTH/2 ? (st.x+end.x)/2 : UI.COND_WDTH+10)*(z);
		}
		P[2].y=P[1].y;

		P[3].x=P[2].x;
		P[3].y=end.y-10;

		P[4].x=end.x;
		P[4].y=P[3].y;
	
		P[5].x=end.x;
		P[5].y=end.y;
		

		for (int i = 0; i < 5; i++)
		{
			pWind->DrawLine(P[i].x,P[i].y,P[i+1].x,P[i+1].y);
		}
		DrawVerticalArrow ( end , Selected) ;
	}

	else
		DrawSimpleConnector(st , end , Selected) ;

	
}

void Output::DrawVerticalArrow (Point Top, bool Selected)
{
	if(Selected)	//if stat is selected, it should be highlighted
			pWind->SetPen(UI.HiClr,3);
		//use highlighting color
	else
		pWind->SetPen(UI.DrawClr,3);  
		//use normal color
	
	pWind->DrawTriangle(Top.x, Top.y , Top.x - 5, Top.y-5, Top.x + 5, Top.y-5) ;
}

void Output :: DrawHorizontalArrow (Point Top, bool selected , bool direction)
{
	if(selected)	//if stat is selected, it should be highlighted
		pWind->SetPen(UI.HiClr,3);
		//use highlighting color
	else
		pWind->SetPen(UI.DrawClr,3);
		//use normal color

	if (direction)
		pWind->DrawTriangle(Top.x , Top.y , Top.x - 5 , Top.y-5 , Top.x - 5 , Top.y+5 ) ;
	else 
		pWind->DrawTriangle(Top.x , Top.y , Top.x + 5 , Top.y-5 , Top.x + 5 , Top.y+5 ) ;
}


//Print double value
void Output :: printdouble(Point p,double d)
{
	pWind->DrawDouble(p.x , p.y , d) ;
} 

void Output :: printstring (Point p , string s ) 
{
	pWind->DrawString(p.x , p.y , s) ;
}
Output::~Output()
{
	delete pWind ;
}
