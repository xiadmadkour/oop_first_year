#ifndef UI_INFO_H
#define UI_INFO_H

#include "..\CMUgraphicsLib\CMUgraphics.h"
#include "..\DEFS.h"

//User Interface information file.
//This file contains info that is needed by Input and Output classes to
//handle the user interface

__declspec(selectany) //This line to prevent "redefinition error"

struct UI_Info	//User Interface Info.
{
	MODE AppMode;		//Application Mode (design or simulation)
	
	int width, height;	//Window width and height
	int wx, wy;			//Window starting coordinates

	int StBrWdth;		//Status Bar Width
	int TlBrWdth;		//Tool Bar Width
	int MnItWdth;		//Menu Item Width
	
	color DrawClr;		//Drawing color
	color HiClr;		//Highlighting color
	color MsgClr;		//Messages color

	int ASSGN_WDTH;		//Assignment statement default width
	int ASSGN_HI;		//Assignment statement default height
	
	int START_RAD;		//Start/End Circle Radius

	int COND_WDTH;		//Conditional statement default width
	int COND_HI;		//Conditional statement default height

	int CON_WDTH;		//Connector's width
	int CON_HI;			//Connector's height
	int DSN_BUT_NO;
}UI;	//create a single global object UI


struct Point	
{
public:
	int x,y;
	Point(int a=0, int b=0)	//constructor
	{ 
		x=a;	
		y=b;
	}
	
	bool operator ==(const Point P)
	{
		if(P.x == x && P.y == y)
			return true ;
		return false;
	}
};

#endif