#ifndef OUPTUT_H
#define OUPTUT_H

#include "Input.h"

class Output	//The application manager should have a pointer to this class
{
private:	
	window* pWind;	//Pointer to the Graphics Window
	void DrawVerticalArrow(Point , bool Selected=false );
	void DrawHorizontalArrow (Point , bool = false , bool direction = true ) ; 
	void DrawSimpleConnector(Point st , Point end  , bool Selected) ;
public:
	Output();	

	window* CreateWind(int, int, int , int);
	void CreateDesignToolBar();	//Tool bar of the design mode
	void CreateSimulationToolBar();//Tool bar of the simulation mode

	void CreateStatusBar();

	Input* CreateInput(); //creates a pointer to the Input object	
	void ClearStatusBar();	//Clears the status bar
	void ClearDrawArea();	//Clears the drawing area

	// -- Statements Drawing Functions
	void DrawAssign(Point Left, int width, int height, string Text, bool Selected=false);
	void DrawReadWrite(Point Left, int width, int height, string Text, bool Selected=false);
	void DrawStartEnd(Point Center, int radius, string Text, bool Selected=false);
	void DrawConditional(Point Left, int width, int height, string Text, bool Selected=false);
	void DrawConnector(Point  , Point , bool Selected=false);
	///Make similar functions for drawing all other statements.

	void printdouble(Point , double d);
	void PrintMessage(string msg);	//Print a message on Status bar
	void printstring (Point , string ) ;
	~Output();
};

#endif