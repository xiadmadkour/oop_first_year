#ifndef DEFS_H
#define DEFS_H

//This file contais some global constants and definitions to be used in the project.

enum ActionType //The actions supported (you can add more if needed)
{
	ADD_SMPL_ASSIGN,//Add simple assignment statement
	ADD_VAR_ASSIGN,	//Add variable assignment statement
	ADD_CONDITION, //Add a conditional statement (for if and while-loop statements)

	ADD_CONNECTOR, //Add a connector between two statements
	ADD_ReadWrite, //Add a paralleogram
	ADD_StartEnd,  //Add a circle
	ADD_COMMENT,
	EDIT_STAT,	//Edit a statement
	EDIT_CONNECTOR,	//Edit a connector

	SELECT,		//Select a statement, a connector

	CUT,
	COPY,
	PASTE,
	DEL,		//Delete a figure(s)
	MOVE,		//Move a figure(s)
	RESIZE,		//Resize a figure(s)
	DRAWING_AREA , // drawing area 
	SAVE,		//Save the whole graph to a file
	LOAD,		//Load a graph from a file
	EXIT,		//Exit the application
	STATUS,		//A click on the status bar
	DSN_TOOL,	//A click on an empty place in the design tool bar
	SIM_TOOL,	//A click on an empty place in the design tool bar
	DSN_MODE,	//Switch to Design mode
	SIM_MODE,	//Switch to simulatiom mode
	SIMULATE,
	STOP,
	GENERATE
};

enum  MODE	//Modes of operation
{
	DESIGN,
	SIMULATION
};


#ifndef NULL
#define NULL 0
#endif

#endif